import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ServicesComponent } from '../services/services.component';
import { CoreComponent } from '../core/core/core.component';

@Component({
  selector: 'app-emp-add-edit',
  templateUrl: './emp-add-edit.component.html',
  styleUrls: ['./emp-add-edit.component.scss']
})
export class EmpAddEditComponent implements OnInit {

  empForm: FormGroup;

  education: string[] = [
    'TonDucThang',
    'TranDaiNghia',
    'BachKhoa',
    'CaoDangVienThong',
    'GiaoThongVanTai',
    'CongNgheThongTin'
  ]

  constructor(
    private _fb: FormBuilder,
    private _service: ServicesComponent,
    private _dialogRef: MatDialogRef<EmpAddEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _coreService: CoreComponent,
  ) {
    this.empForm = this._fb.group({
      firstName: '',
      lastName: '',
      email: '',
      dob: '',
      gender: '',
      education: '',
      company: '',
      experience: '',
      package: '',
    })
  }

  ngOnInit(): void {
    this.empForm.patchValue(this.data)
  }

  onFormSubmit() {
    if (this.empForm.valid) {
      console.log(this.empForm.value);
      if (this.data) {
        this._service.updateEmployee(this.data.id, this.empForm.value).subscribe({
          next: (val: any) => {
            // alert('user update successfully!')
            this._coreService.openSnackBar('Employee updated!')
            this._dialogRef.close(true)
          },
          error: (err: any) => {
            console.log(err);
          }
        })
      } else {
        this._service.addEmployee(this.empForm.value).subscribe({
          next: (val: any) => {
            // alert('user add successfully!')
            this._coreService.openSnackBar('Employee added!')
            this._dialogRef.close(true)
          },
          error: (err: any) => {
            console.log(err);
          }
        })
      }
    }
  }
}
