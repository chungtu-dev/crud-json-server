import { HttpClient } from '@angular/common/http';
import { Component, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent {
  constructor(private _http: HttpClient) { }

  addEmployee(data: any): Observable<any> {
    return this._http.post('http://localhost:8000/employees', data)
  }

  updateEmployee(id: number, data: any): Observable<any> {
    return this._http.put(`http://localhost:8000/employees/${id}`, data)
  }

  getEmployeeList(): Observable<any> {
    return this._http.get('http://localhost:8000/employees')
  }

  deleteEmployee(id: number): Observable<any> {
    return this._http.delete(`http://localhost:8000/employees/${id}`)
  }
}
